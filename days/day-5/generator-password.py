#Password Generator Project
import random
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
symbols = ['!', '#', '$', '%', '&', '(', ')', '*', '+']

print("Welcome to the PyPassword Generator!")
nr_letters= int(input("How many letters would you like in your password?\n")) 
nr_symbols = int(input(f"How many symbols would you like?\n"))
nr_numbers = int(input(f"How many numbers would you like?\n"))

#Eazy Level - Order not randomised:
#e.g. 4 letter, 2 symbol, 2 number = JduE&!91
total_letras = len(letters)
letras_final = ''
for i in range(0, nr_letters):
  letras = random.randint(0, total_letras-1)
  letras_final += (letters[letras])

total_simbolos = len(symbols)
simbolos_final = ''
for i in range(0, nr_symbols):
  simbolo = random.randint(0, total_simbolos-1)
  simbolos_final += (symbols[simbolo])

total_nummeros = len(numbers)
numeros_final = ''
for i in range(0, nr_numbers):
  numero = random.randint(0, total_nummeros-1)
  numeros_final += (numbers[numero])

print(f"Password: {letras_final}{simbolos_final}{numeros_final}")



#Hard Level - Order of characters randomised:
#e.g. 4 letter, 2 symbol, 2 number = g^2jk8&P

lista_final = []
total_letras = len(letters)
letras_final = []
for char in range(1, nr_letters + 1):
  lista_final.append(random.choice(letters))

for char in range(1, nr_symbols + 1):
  lista_final += random.choice(symbols)

for char in range(1, nr_numbers + 1):
  lista_final += random.choice(numbers)

print(lista_final)
random.shuffle(lista_final)
print(lista_final)

password = ""
for char in lista_final:
  password += char

print(f"Your password is: {password}")


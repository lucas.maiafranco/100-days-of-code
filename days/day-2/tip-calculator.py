#If the bill was $150.00, split between 5 people, with 12% tip. 
#Each person should pay (150.00 / 5) * 1.12 = 33.6
#Format the result to 2 decimal places = 33.60
#Tip: There are 2 ways to round a number. You might have to do some Googling to solve this.💪
#HINT 1: https://www.google.com/search?q=how+to+round+number+to+2+decimal+places+python&oq=how+to+round+number+to+2+decimal
#HINT 2: https://www.kite.com/python/answers/how-to-limit-a-float-to-two-decimal-places-in-python
print("Bem vindo a caluladora de gorjetas!")
valor_conta = float(input("Qual o valor tota da conta? R$"))
gorjeta = int(input("Qual a porcentagem de gorjeta? 10, 12 ou 15?"))
quantida_pessoas = int(input("Por quantas pessoas vai dividir a conta?"))

gorjeta_porcentagem = gorjeta / 100
valor_gorjeta = valor_conta * gorjeta_porcentagem
total_conta = valor_conta + valor_gorjeta
valor_por_pessoa = total_conta / quantida_pessoas
valor_final = round(valor_por_pessoa, 2)

print(f"Cada pessoa deve pagar R${valor_final}")

rock = '''
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''

import random
jogador1 = int(input("Qual sua escolha? 0 - pedra, 1 - papel, 2 - tesoura \n"))
if jogador1 == 0:
  print(rock)
elif jogador1 == 1:
  print(paper)
elif jogador1 == 2:
  print(scissors)

computador = random.randint(0, 2)
print("Escolha do computador: ")
if computador == 0:
  print(rock)
elif computador == 1:
  print(paper)
elif computador == 2:
  print(scissors)

if jogador1 == 0:
  if computador == 1:
    print("Voce Perdeu!")
  elif computador == 2:
    print("Voce Ganhou!")
  else:
    print("Empatou")
elif jogador1 == 1:
  if computador == 0:
    print("Voce Ganhou!")
  elif computador == 2:
    print("Voce Perdeu!")
  else:
    print("Empatou")
elif jogador1 == 2:
  if computador == 0:
    print("Voce Perdeu!")
  elif computador == 1:
    print("Voce Ganhou!")
  else:
    print("Empatou") 
